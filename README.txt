Commerce wayforpay
===============

CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Author
  * Similar projects and how they are different

INTRODUCTION
------------
This module provides a Drupal Commerce payment method to embed the payment
services provided by Wayforpay

It efficiently integrates payments from various sources such as:
- credit cards;
- cash via self-service terminals (offline payments);
- privat24 banking.

REQUIREMENTS
------------

Commerce payment

INSTALLATION
------------

1. Install module as usual via Drupal UI, Drush or Composer.
2. Go to "Extend" and enable the Commerce wayforpay module.

CONFIGURATION
----------------

1. Add "Wayforpay" in  payment gateways  page
/admin/commerce/config/payment-gateways
2. Enjoy

AUTHOR
------

shmel210
Drupal: (https://www.drupal.org/user/2600028)
Email: shmel210@zina.com.ua

Company: Zina Design Studio
Website: (http://zina.com.ua)
Drupal: (https://www.drupal.org/user/361734/)
Email: info@zina.com.ua

SIMILAR PROJECTS AND HOW THEY ARE DIFFERENT
-------------------------------------------
There is no similar projects at this time.
